# Los casos de uso

## ¿Qué es un caso de uso?

Un caso de uso es una descripción detallada de cómo un usuario interactúa con un sistema para realizar una tarea específica. Incluye los pasos necesarios y los resultados esperados de la interacción.

## Componentes de un caso de uso

* **Título:** Un nombre claro y descriptivo del caso de uso.
* **Actor:** La persona o sistema que interactúa con el sistema principal.
* **Descripción:** Una breve explicación del objetivo del caso de uso.
* **Precondiciones:** Condiciones que deben cumplirse antes de que el caso de uso pueda iniciarse.
* **Flujo Principal:** Los pasos secuenciales que sigue el actor para completar la tarea con éxito.
* **Flujos Alternativos:** Variaciones en el flujo principal, incluyendo errores y excepciones.
* **Postcondiciones:** El estado del sistema después de que el caso de uso se haya completado.

### Ejemplo: Sistema de Biblioteca

* **Título:** Préstamo de un libro
* **Actor:** Usuario (Estudiante)
* **Descripción:** El usuario desea tomar prestado un libro de la biblioteca.

**Precondiciones:**

* El usuario debe estar registrado en el sistema de la biblioteca.
* El usuario debe haber iniciado sesión en su cuenta.

**Flujo Principal:**

* El usuario navega al catálogo de libros en línea.
* El usuario busca el libro deseado usando la barra de búsqueda.
* El usuario selecciona el libro de la lista de resultados.
* El usuario hace clic en el botón "Prestar".
* El sistema verifica la disponibilidad del libro.
* El sistema registra el préstamo y actualiza la base de datos.
* El sistema confirma el préstamo y proporciona la fecha de devolución.

**Flujos Alternativos:**

* **Libro no disponible:** El sistema muestra un mensaje indicando que el libro no está disponible y su fecha estimada de devolución.
* **Error en la conexión:** El sistema muestra un mensaje de error y sugiere intentar más tarde.

**Postcondiciones:**

* El libro está registrado como prestado al usuario en la base de datos.
* El usuario recibe una confirmación del préstamo con la fecha de devolución.

## Tipos de Relaciones en Casos de Uso

En un diagrama de casos de uso, las relaciones representan cómo interactúan los actores y los casos de uso entre sí. Aquí están los tipos principales de relaciones:

**Inclusión (Include):**

* **Descripción:** Un caso de uso incluye el comportamiento de otro caso de uso. Es como decir "este caso de uso siempre hace lo que ese otro caso de uso hace".

* **Ejemplo:** 

* "Verificar la disponibilidad del libro" se incluye en "Prestar un libro".
* "Verificar la disponibilidad del libro" se incluye en "Renovar un libro".
* "Extender el período de préstamo y actualizar la base de datos" se incluye en "Renovar un libro".

**Extensión (Extend):**

* **Descripción:** Un caso de uso extiende el comportamiento de otro caso de uso. Es como decir "este caso de uso puede hacer algo adicional en ciertas condiciones".

* **Ejemplo:** 

* "Gestionar renovación de libro raro" extiende "Renovar un libro".

**Generalización (Generalize):**

* **Descripción:** Un caso de uso puede ser una especialización de otro caso de uso. Es como decir "este caso de uso es una versión más específica de ese otro caso de uso".

**Ejemplo:** 

* "Gestionar renovación de libro raro" es una especialización de "Renovar un libro".

# Ejemplo del diagrama de casos de usos

![dcu](/casos_uso/use-case-diagram.png)

## Actividad Práctica:

Desarrollar los diagramas de casos de uso de vuestro sistema a desarrollar.